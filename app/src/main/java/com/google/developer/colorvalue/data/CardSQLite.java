package com.google.developer.colorvalue.data;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.util.Log;

import com.google.developer.colorvalue.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static com.google.developer.colorvalue.data.CardProvider.Contract.*;
import static com.google.developer.colorvalue.data.CardProvider.Contract.Columns.*;

/**
 * Helper class to manage database
 */
public class CardSQLite extends SQLiteOpenHelper {

    private static final String TAG = CardSQLite.class.getName();
    private static final String DB_NAME = "colorvalue.db";
    private static final int DB_VERSION = 1;

    private Resources mResources;

    private static final String CREATE_CARDS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "("
            + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLOR_HEX + " TEXT, "
            + COLOR_NAME + " TEXT " + ")";

    private static final String CHECK_CARDS_TABLE = "SELECT count(*) from " + TABLE_NAME;

    public CardSQLite(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        mResources = context.getResources();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_CARDS_TABLE);
        Cursor cursor = db.rawQuery(CHECK_CARDS_TABLE, null);
        cursor.moveToFirst();
        if(cursor.getInt(0) == 0) {
            Log.d("godammit", "Creating database");
            addDemoCards(db);
        }
        cursor.close();
    }

    /**
     * save demo cards into database
     */
    private void addDemoCards(SQLiteDatabase db) {
        try {
            db.beginTransaction();
            try {
                readCardsFromResources(db);
                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
            }
        } catch (IOException | JSONException e) {
            Log.e(TAG, "Unable to pre-fill database", e);
        }
    }

    /**
     * load demo color cards from {@link raw/colorcards.json}
     */
    private void readCardsFromResources(SQLiteDatabase db) throws IOException, JSONException {
        StringBuilder builder = new StringBuilder();
        InputStream in = mResources.openRawResource(R.raw.colorcards);
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));

        String line;
        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }

        fillCardsIntoDb(builder.toString(), db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public static String getColumnString(Cursor cursor, String name) {
        return cursor.getString(cursor.getColumnIndex(name));
    }

    public static int getColumnInt(Cursor cursor, String name) {
        return cursor.getInt(cursor.getColumnIndex(name));
    }

    private void fillCardsIntoDb(String jsonData, SQLiteDatabase db)  throws JSONException {
        //Parse resource into key/values
        JSONObject cardsJson = new JSONObject(jsonData);
        JSONArray cardsArray = cardsJson.getJSONArray("cards");
        JSONObject cardObject;

        for (int i = 0; i < cardsArray.length(); i++) {
            cardObject = cardsArray.getJSONObject(i);
            ContentValues values = new ContentValues();
            values.put(COLOR_HEX, cardObject.getString("hex"));
            values.put(COLOR_NAME, cardObject.getString("name"));
            db.insert(TABLE_NAME, null, values);
        }
    }

    public Cursor getCards() {
        return getReadableDatabase().query(TABLE_NAME, null, null,
                        null, null, null, null);
    }

    public Cursor getCardsByID(Uri uri) {
        return getReadableDatabase()
                .query(TABLE_NAME, null, _ID + "=" + uri.getLastPathSegment(),
                        null, null, null, null);
    }

    public long insert(ContentValues values) {
        return getWritableDatabase()
                .insert(TABLE_NAME, null, values);
    }

    public int delete(Uri uri) {
        return getWritableDatabase()
                .delete(TABLE_NAME, _ID + "=" + uri.getLastPathSegment(), null);
    }
}
