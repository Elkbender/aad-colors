package com.google.developer.colorvalue;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.developer.colorvalue.data.Card;
import com.google.developer.colorvalue.data.CardProvider;
import com.google.developer.colorvalue.service.CardService;

public class CardActivity extends AppCompatActivity {
    private static final String TAG = CardActivity.class.getSimpleName();
    private static final String EXTRA_CARD = "EXTRA_CARD";
    private Card card;

    public static Intent getIntent(Context context, Card card) {
        Intent intent = new Intent(context, CardActivity.class);
        intent.putExtra(EXTRA_CARD, card);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card);
        card = getIntent().getParcelableExtra(EXTRA_CARD);
        setCardDetails(card);
    }

    public void setCardDetails(Card card) {
        setColor(card.getHex());
        setName(card.mName);
        setHexString(card.getHex());
    }

    private void setColor(String hex) {
        View view = findViewById(R.id.color);
        view.setBackgroundColor(Color.parseColor(hex));
    }

    private void setName(String name) {
        TextView view = findViewById(R.id.color_name);
        view.setText(name);
    }

    private void setHexString(String hex) {
        TextView view = findViewById(R.id.color_hex);
        view.setText(hex);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_delete, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_delete) {
            CardService.deleteCard(this, card.getUri());
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
